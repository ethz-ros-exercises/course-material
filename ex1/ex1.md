
## Install Husky simulator in Kinetic
The command
`sudo apt-get install ros-kinetic-husky-simulator`
does not work.

Compile it from source:

```.sh
mkdir ~/github
cd ~/github

git clone https://github.com/husky/husky.git
git clone https://github.com/husky/husky_simulator.git
git clone https://github.com/husky/husky_desktop.git

mkdir -p ~/workspace/src
cd ~/workspace/src
ln -s ~/github/husky .
ln -s ~/github/husky_simulator .
ln -s ~/github/husky_desktop .
cd ..
catkin init
catkin build
source devel/setup.bash

sudo apt-get install ros-kinetic-gazebo-ros-pkgs ros-kinetic-gazebo-ros-control
sudo apt-get install ros-kinetic-multimaster-launch
sudo apt-get install ros-kinetic-lms1xx
rosdep install --from-path src --ignore-src
catkin build
source devel/setup.bash

# No need to setup the HUSKY_* environement here.
# It has been set in "source devel/setup.bash"

roslaunch husky_gazebo husky_empty_world.launch
```

Refer to
https://answers.ros.org/question/256756/how-to-install-husky-simulator-in-kinetic/

## Show the type of the topic

```.sh
$ rostopic type /turtle1/cmd_vel
geometry_msgs/Twist

$ rosmsg show geometry_msgs/Twist
geometry_msgs/Vector3 linear
  float64 x
  float64 y
  float64 z
geometry_msgs/Vector3 angular
  float64 x
  float64 y
  float64 z

$ rostopic type /turtle1/cmd_vel | rosmsg show
geometry_msgs/Vector3 linear
  float64 x
  float64 y
  float64 z
geometry_msgs/Vector3 angular
  float64 x
  float64 y
  float64 z
```

## Publish

```.sh
rostopic pub /turtle1/cmd_vel $(rostopic type /turtle1/cmd_vel) -r 5 [1,0,0] [0,0,1]
```

## Install teleop_twist_keyboard from source

```.sh
cd ~/github
git clone https://github.com/ros-teleop/teleop_twist_keyboard.git

cd ~/workspace/src
ln -s ~/github/teleop_twist_keyboard .
cd ..
catkin build teleop_twist_keyboard
source devel/setup.bash
```

To verify that it is indeed compiled from source

```.sh
$ roscd teleop_twist_keyboard/
$ pwd
/home/xxx/workspace/src/teleop_twist_keyboard
```

## Use teleop_twist_keyboard to control the turtle

```.sh
rosrun turtlesim turtlesim_node turtle1/cmd_vel:=cmd_vel
rosrun teleop_twist_keyboard teleop_twist_keyboard.py
```

Since the topic published by `teleop_twist_keyboard`
is `/cmd_vel`, we have remapped it above
from `/turtle1/cmd_vel` to `/cmd_vel`.

Press the following keys to control the turtle

 - `u`: Move counter-clockwise
 - `j`: Turn counter-clockwise
 - `m`: Move clockwise
 - `i`: Move forward
 - `,`: Move backward
 - `o`: Move clockwise
 - `l`: Turn clockwise
 - `.`: Turn counter-clockwise

Use the following launch file to
start the two nodes at once

```.xml
<launch>
  <node pkg="turtlesim" type="turtlesim_node" name="turtlesim">
    <remap from="turtle1/cmd_vel" to="cmd_vel"/>
  </node>

  <node pkg="teleop_twist_keyboard" type="teleop_twist_keyboard.py"
        name="teleop_twist_keyboard"
        launch-prefix="xterm -e"
  />
</launch>
```

## Use husky simulator and teleop_twist_keyboard
Refer to the file `husky_empty_world.launch`, it says
that the `world_name` is relative to the environment
variable `GAZEBO_RESOURCE_PATH`. Therefore, we
first set this variable

```.sh
export GAZEBO_RESOURCE_PATH=/usr/share/gazebo-7
```

Then launch the following launch file

```.xml
<launch>
  <arg name="world_name"
       default="worlds/stacks.world"
  />

  <include file="$(find husky_gazebo)/launch/husky_empty_world.launch">
    <arg name="world_name" value="$(arg world_name)" />
  </include>
  <node pkg="teleop_twist_keyboard" type="teleop_twist_keyboard.py"
        name="teleop_twist_keyboard"
        launch-prefix="xterm -e"
  />

</launch>
```

Press the keys described above to move the car.

